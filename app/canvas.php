<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="lib/modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="lib/modules/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="lib/site/css/site.css" rel="stylesheet">
    <link href="lib/site/css/<?=$pageMode?>.css" rel="stylesheet">
    <script src="lib/modules/jquery/dist/jquery.min.js"></script>
    <script src="lib/modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="lib/site/js/<?=$pageMode?>.js"></script>
    <title><?=Site::Title?></title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark text-white bg-dark" id="header">
      <div class="container-fluid">
        <div class="navbar flex-nowrap w-100">
          <ul class="navbar navbar-nav w-100">
            <li class="nav-item">
            </li>
            <li class="nav-item">
              <h2><?=Site::Title?></h2>
            </li>
            <li class="nav-item">
              <?php if($pageMode == 'main') { ?>
                <div class="dropdown">
                  <a class="btn btn-dark" href="javascript:void(0)" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    <span class="fas fa-icon fa-2x fa-user" aria-hidden="true"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-menu-end"">
                    <li class="dropdown-item-text text-nowrap">
                      <strong><?=$_SESSION['displayName']?></strong><br />
                      <i class="text-muted extra-small" id="user-email-addresses" data-value="<?=implode(':', $_SESSION['emailAddresses'])?>"><?=implode(' / ', $_SESSION['emailAddresses'])?></i>
                    </li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="javascript:void(0)" id="logout-link"><i class="fas fa-sign-out-alt pe-2"></i>Sair</a></li>
                  </ul>
                </div>
              <?php } ?>
            </li>   
          </ul>
          <?php if($pageMode == 'login') { ?>
            <div class="align-self-end extra-small"><i>v.<?=Site::Version?></i></div>
          <?php } ?>
        </div>    
      </div>
    </nav><!--Header-->

    <?php 
      if(isset($pageMode)) {
        require('app/' . $pageMode . '.php');
       }
    ?>

    <!-- Modal Dialog -->
    <div class="modal" tabindex="-1" id="modal-dialog">
      <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title d-flex align-items-center"><i></i><span></span></h5>
          </div>
          <div class="modal-body"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="modal-dialog-confirm-button" data-bs-dismiss="modal"></button>
            <button type="button" class="btn btn-secondary" id="modal-dialog-abort-button" data-bs-dismiss="modal"></button>
          </div>
        </div>
      </div>
    </div>

  </body>

</html>
