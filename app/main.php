    <main>
      <nav class="sidebar d-flex flex-column flex-shrink-0 p-3 text-white bg-dark">
        <ul class="nav nav-pills flex-column mb-auto">
          <li class="nav-item">
            <a href="javascript:void(0)" class="nav-link text-white active text-nowrap" data-show="ticket-list">
              <i class="fas fa-home pe-2"></i>
              In&iacute;cio
            </a>
          </li>
          <li>
            <a href="javascript:void(0)" class="nav-link text-white text-nowrap" data-show="new-ticket">
              <i class="fas fa-concierge-bell pe-2"></i>
              Nova Solicita&ccedil;&atilde;o
            </a>
          </li>
        </ul>
      </nav><!--Sidebar-->

      <div class="container-fluid overflow-auto pt-2 main-content" id="main-ticket-list">

        <?php if(! empty($message)) { ?>
          <div class="alert alert-<?=$message['type']?> alert-dismissible fade show d-flex align-items-center mb-2" id="alert">
            <i class="fa fa-2x fa-<?=$message['icon']?> me-3"></i>
            <div><?=$message['text']?></div>
            <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
          </div>
        <?php } ?>

        <div id="main-ticket-list-contents"></div>

        <div class="card my-1 text-nowrap" id="ticket-list-load-progress">
          <div class="card-header text-center">
              Carregando...
          </div>
          <div class="card-body">
            <div class="progress">
              <div class="progress-bar" style="width: 0%;"></div>
            </div>
          </div>
        </div>

        <div class="card my-1 text-nowrap" id="ticket-list-load-more">
          <div class="card-body">
            <button type="button" class="btn btn-sm btn-block btn-success">Mais</button>
          </div>
        </div>

        <div id="tickets-filters">
          <div  class="card collapse" >
            <div class="card-body">
              <h6 class="card-title">Exibir</h6>
                <?php
                  foreach(Util::TicketStatus as $status) { 
                    $active = in_array($status, $_SESSION['ticketStatusFilter']);
                ?>
                <div class="badge w-100 my-1 ticket-bg-<?=$status?>" id="tickets-filters-<?=$status?>" data-state="<?=$active ? '1' : '0'?>">
                  <i class="icon fas fa-<?=$active ? 'eye' : 'eye-slash'?> pe-2"></i>
                  <span><?=Util::StatusName($status)?></span>
                </div>
              <?php } ?>
           </div>
          </div>
          <a href="#" class="button bg-primary" data-bs-toggle="collapse" data-bs-target="#tickets-filters .card">
            <i class="fas fa-eye text-white"></i>
          </a>
        </div>
      </div>

      <div class="container-fluid overflow-auto main-content" id="main-new-ticket">

        <div class="accordion mt-3" id="accordion-new-ticket">

          <?php $n = 1; foreach($config->topics() as $topicCategory => $topicCategoryData) { ?>
            <div class="accordion-item">
              <h2 class="accordion-header">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-<?=$n?>">
                  <i class="fas fa-<?=$topicCategoryData['icon']?> pe-2"></i><?=$topicCategory?>
                </button>
              </h2>
              <div id="collapse-<?=$n?>" class="accordion-collapse collapse">
                <div class="accordion-body">
                  <ul class="list-group-flush ps-0">
                    <?php foreach($topicCategoryData['topics'] as $topicName => $topicData) { ?>
                      <li class="list-group-item ps-1" data-new-ticket-queue="<?=$topicData['queueId']?>" data-new-ticket-subject="<?=$topicData['defaultSubject'] ?? ''?>"><?=$topicName?>
                        <?php if(isset($topicData['description'])) { ?>
                          <div class="form-text"><?=$topicData['description']?></div>
                        <?php } ?>
                      </li>
                    <?php } ?>
                  </ul>
                </div>
              </div>
            </div>
          <?php $n++; } ?>

        </div><!--accordion-->
      
      </div><!--container main-new-ticket-->

      <div class="container-fluid main-content" id="main-form">
        <form method="POST" class="custom-form-validation" enctype="multipart/form-data" novalidate>
          <div class="card my-3">
            <div class="card-header">
              <h4 id="main-form-title"></h4>
            </div>
            <div class="card-body" id="main-form-body">
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary" id="btn-main-form-submit">Enviar</button>
              <button type="button" class="btn btn-secondary" id="btn-main-form-back">Voltar</button>
            </div>
          </div>
          <input type="hidden" id="main-form-mode" name="form_mode">
          <input type="hidden" id="main-form-new-ticket-queue" name="queue">
          <input type="hidden" id="main-form-new-message-ticket" name="ticket">
        </form>
      </div>
  
    </main>


    <!-- Modal Message Viewer -->
    <div class="modal" tabindex="-1" id="modal-message-viewer">
      <div class="modal-dialog modal-dialog-scrollable modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <div class="containter">
              <h5 class="modal-title d-flex align-items-center">Detalhes da Mensagem</h5>
              <div class="message-viewer-subtitle text-secondary"><i></i></div>
            </div>
            <button type="button" class="btn-close" data-bs-dismiss="modal"></button>              
          </div>
          <div class="modal-body">
            <div class="text-center text-primary message-body-spinner">
              <div class="spinner-border" role="status"></div>
            </div>
            <div class="message-body"></div>
          </div>
          <div class="modal-footer">
            <div class="dropdown dropup message-attachment-list">
              <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown">
                Anexos
              </button>
              <ul class="dropdown-menu">
              </ul>
            </div>
            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Fechar</button>
          </div>
        </div>
      </div>
    </div>

