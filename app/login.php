    <main>
      <div class="container-fluid">

        <div class="d-flex justify-content-center">

          <div class="card" id="login-form">
            <div class="card-body">
              <form method="POST">
                <div class="d-flex flex-column justify-content-center">
                  <h2 class="mt-3 mx-auto">Login</h2>
                  <?php if(! empty($message)) { ?>
                    <p class="my-0 py-0 mx-auto message-<?=$message['type']?>"><i class="fa fa-<?=$message['icon']?> pe-1"></i><?=$message['text']?></p>
                  <?php } else { ?>
                    <p class="my-0 py-0 mx-auto text-center message-default"><small>
                      Informe seu nome de usu&aacute;rio e senha do IMECC<br />
                      ou clique <a class="link-secondary" href="<?=$phoenixAuthUrl?>">aqui</a> para entrar com sua conta da Unicamp</small></p>
                  <?php } ?>
                </div>
                <div class="input-group my-3 px-2">
                  <span class="fas fa-user input-group-text bg-white border-end-0 pt-2"></span>
                  <input type="text" class="form-control border-start-0 ps-0" id="login-form-username" name="username" placeholder="Nome de usu&aacute;rio" value="<?=$_POST['username'] ?? null?>">
                </div>
                <div class="input-group my-3 px-2">
                  <span class="fas fa-lock input-group-text bg-white border-end-0 pt-2"></span>
                  <input type="password" class="form-control border-start-0 ps-0" id="login-form-password" name="password" placeholder="Senha">
                </div>
                <div class="d-grid my-3 px-2">
                  <button class="btn btn-primary" type="submit" disabled>Entrar</button>
                </div>
                <input type="hidden" id="login-form-mode" name="form_mode" value="login">
              </form>
            </div>
            <div class="card-footer form-text text-center py-2">
              <i class="fa fa-info-circle pe-1"></i>Este sistema acompanha apenas solicita&ccedil;&otilde;es <br />
              enviadas de e-mails <i>@ime.unicamp.br</i> e <i>@unicamp.br</i>
            </div>
          </div>
        </div>

      </div><!--container-->

    </main>
