<?php

class Util {

  const TicketStatus = array('new', 'open', 'stalled', 'waiting', 'rejected', 'resolved', 'deleted');

  static function StatusName($rtState) {
    switch($rtState) {
      case 'new':
        return 'Criado';
      case 'open':
        return 'Em Atendimento';
      case 'resolved':
        return 'Concluído';
      case 'stalled':
        return 'Pendente';
      case 'waiting':
        return 'Aguardando Solicitante';
      case 'rejected':
        return 'Rejeitado';
      case 'deleted':
        return 'Removido';  
      default:
        return strtoupper($rtState);
    }
  }

  static function ElapsedTime($date) {
    $delta = array();
    $date = new DateTime($date);
    $now = new DateTime('UTC');
    $diff = $date->diff($now);
    if($diff->y > 0) $delta[] = "$diff->y ano" . ($diff->y > 1 ? 's' : '');
    if($diff->m > 0) $delta[] = "$diff->m " . ($diff->m > 1 ? 'meses' : 'm&ecirc;s');
    if($diff->d > 0) $delta[] = "$diff->d dia" . ($diff->d > 1 ? 's' : '');
    $str = array_pop($delta);
    if($delta) {
      $delta = implode(', ', $delta) . ' e ' . $str;
    } else {
      $delta = $str;
    }
    return empty($delta) ? 'hoje' : $delta;
  }

}

?>

