<?php

class Phoenix {
  
  private $config;

  function __construct($config) {
    $this->config = $config;
    array_walk_recursive($this->config, function(&$value, $key) { $value = strtolower($value); });
  }

  private function getHostCertificate($url) {      
    $ctx = stream_context_create(array(
      'ssl' => array(
        'capture_peer_cert' => true
      )
    ));
    if($remoteFd = @fopen($url, 'r', false, $ctx)) {
      $remoteData = stream_context_get_params($remoteFd);
      fclose($remoteFd);
      return $remoteData['options']['ssl']['peer_certificate'];
    }
    return false; 
  }

  function decrypt($data) {
    if($hostCertificate = $this->getHostCertificate($this->config['url'])) {
        $publicKey = openssl_get_publickey($hostCertificate);
        if(openssl_public_decrypt(base64_decode($data), $result, $publicKey)) {
          return(json_decode($result, true));
        }
    }
    return false;
  }

}

?>
