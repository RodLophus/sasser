<?php

class Ldap {

  private $config;
  private $userCache = array();
  private $groupCache = array();

  function __construct($config) {
    $this->config = $config;
    array_walk_recursive($this->config, function(&$value, $key) {$value = strtolower($value); });
  }

  private function query($base, $filter, $attributes) {
    $values = null;
    if($ldap = ldap_connect($this->config['server_uri'])) {
      ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
      if(ldap_bind($ldap)) {
        if($result = @ldap_search($ldap, $base, $filter, $attributes)) {
          $entries = ldap_get_entries($ldap, $result);
          if($entries['count'] > 0) {
            foreach($attributes as $attribute) {
              if(isset($entries[0][$attribute][0])) $values[$attribute] = $entries[0][$attribute][0];
            }
          }
        }
        ldap_close($ldap);
      }
    }
    return $values;
  }

  private function filter($attribute, $value) {
    return '(' . $this->config[$attribute . '_attribute'] . '=' . $value . ')';
  }

  function getGroupName($gid) {
    if(isset($this->groupCache[$gid])) {
      return $this->groupCache[$gid];
    }
    if($result = $this->query($this->config['group_base_dn'], $this->filter('group_gid', $gid), array($this->config['group_name_attribute']))) {
      $result = array_values($result)[0];
      $this->groupCache[$gid] = $result;
      return $result;
    }
    return null;
  }

  function getUser($login) {
    if(isset($this->userCache[$login])) {
      return $this->userCache[$login];
    }
    $attributes = array();
    foreach($this->config as $key => $value) {
      if(preg_match('/^user_\w+_attribute/', $key)) {
        if(is_array($value)) {
          $attributes = array_merge($attributes, $value);
        } else {
          $attributes[$key] = $value;
        }
      }
    }
    if($result = $this->query($this->config['user_base_dn'], $this->filter('user_login', $login), array_values($attributes))) {
      $data = new stdClass();
      $data->login = $login;
      $data->name = $result[$this->config['user_name_attribute']];
      $data->group = $this->getGroupName($result[$this->config['user_gid_attribute']]);
      $data->emailAddresses = array();
      foreach($this->config['user_email_attributes'] as $attribute) {
        if(isset($result[$attribute])) $data->emailAddresses[] = $result[$attribute];
      }
      $this->userCache[$login] = $data;
      return $data;
    }
    return null;
  }

}

?>