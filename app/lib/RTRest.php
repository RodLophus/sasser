<?php

define('FREAD_CHUNK_SIZE', 4096);

Class RTRest {

  private $restUrl;
  private $authToken;
  private $referer;
  public $ticketsPerPage;

  private $userCache = array();
  private $queueCache = array();

  function __construct($config) {
    $this->restUrl   = $config['url'] . '/REST/2.0/';
    $this->authToken = $config['auth_token'];
    $this->referer   = $config['referer'];
    $this->ticketsPerPage = 5;
  }

  private function query($path, $data = null) {
    $ctx = stream_context_create(array(
      'http' => array(
        'method' => ($data ? 'POST' : 'GET'),
        'header' => 'Authorization: token ' . $this->authToken . "\r\n" .
          'Referer: ' . $this->referer . "\r\n" .
          ($data ? "Content-Type: application/json\r\n" : ''),
        'content' => ($data ? json_encode($data) : '')
      )
    ));
    $data = null;
    if($rest = @fopen($this->restUrl . $path, 'r', false, $ctx)) {
      while(! feof($rest)) {
        $data .= fread($rest, FREAD_CHUNK_SIZE);
      }
      $data = json_decode($data);
      fclose($rest);
    }
    return $data;
  }

  private function sqlFilter($query = array()) {
    if(empty($query)) return null;
    $filter = array();
    foreach($query as $parameter => $values) {
      if(! is_array($values)) $values = array($values);
      $filter[] = '(' . implode(' OR ', array_map(function($v) use ($parameter) { return "${parameter}='$v'"; }, $values)) . ')';
    }
    return implode(' AND ', $filter);
  }

  private function jsonFilter($query = array()) {
    $filter = array();
    if(! empty($query)) {
      foreach($query as $parameter => $values) {
        if(! is_array($values)) $values = array($values);
        foreach($values as $value) {
          $filter[] = array(
            'field' => $parameter,
            'value' => $value
          );
        }
      }
    }
    return $filter;
  }
  
  function createTicket($data) {
    return $this->query('ticket', $data);
  }

  function replyTicket($id, $data) {
    return $this->query("ticket/$id/correspond", $data);
  }

  function getTickets($filter, $page = 1) {
    $ticketsPerPage = $this->ticketsPerPage;
    $filter = urlencode($this->sqlFilter($filter));
    return $this->query("tickets?orderby=id&order=DESC&query=$filter&per_page=$ticketsPerPage&page=$page");
  }

  function getTicket($id) {
    return $this->query("ticket/$id");
  }

  function getAttachment($id) {
    return $this->query("attachment/$id");
  }

  function getTicketTransactions($ticketId, $filter = null) {
    $defaultFilter = array(
      'ObjectType' => 'RT::Ticket',
      'ObjectId'   => $ticketId
    );
    if($filter) {
      $filter = array_merge($defaultFilter, $filter);
    } else {
      $filter = $defaultFilter;
    }
    $filter = urlencode($this->sqlFilter($filter));
    return $this->query("transactions?query=$filter&per_page=100");
  }

  function getTransaction($id) {
    return $this->query("transaction/$id");
  }

  function getTransactionAttachments($id) {
    return $this->query("transaction/$id/attachments");
  }

  function getQueue($id) {
    if(isset($this->queueCache[$id])) {
      return $this->queueCache[$id];
    }
    $result = $this->query("queue/$id");
    $this->queueCache[$id] = $result;
    return $result;
  }

  function getUser($id) {
    if(isset($this->userCache[$id])) {
      return $this->userCache[$id];
    }
    $result = $this->query("user/$id");
    $this->userCache[$id] = $result;
    return $result;
  }

  function getUsersIds($filter) {
    $filter = $this->jsonFilter($filter);
    return array_map(function($u) { return $u->id; }, $this->query('users', $filter)->items);
  }

}

?>