<?php

Class Config {

  const DefaultTicketStatusFilter = ['new', 'open', 'stalled', 'waiting'];
  const DefaultExpressServiceIcon = ['gear', 'lightgray'];
  
  private $config = [];
  
  function __construct($configDir) {
    $config = [];
    foreach(scandir($configDir) as $f) {
      if(is_dir($f)) continue;
      if(! preg_match('/\.conf$/', $f)) continue;
      require_once($configDir . '/' . $f);
    }
    $this->config = $config;
  }

  private function get($section, $value) {
    if($value) {
      return $this->config[$section][$value] ?? null;
    }
    return $this->config[$section] ?? null;
  }

  function common($data = false)  { return $this->get('common',  $data); }
  function rt($data = false)      { return $this->get('rt',      $data); }
  function ldap($data = false)    { return $this->get('ldap',    $data); }
  function phoenix($data = false) { return $this->get('phoenix', $data); }
  function topics($data = false)  { return $this->get('topics',  $data); }

  function express($placeTag, $placeTagVariation = false) {
    $knownServiceGroups = $this->config['serviceGroups'];
    $knownPlaces = $this->config['places'];
    if(! isset($knownPlaces[$placeTag])) return null;
    $result = [];
    if(! $placeTagVariation) $placeTagVariation = '??';
    $result['place'] = str_replace('%%', $placeTagVariation, $knownPlaces[$placeTag]);
    $result['serviceGroups'] = [];
    foreach($knownServiceGroups as $serviceGroupName => $serviceGroupData) {
      $serviceGroup = null;
      foreach($serviceGroupData['places'] as $serviceGroupPlace) {
        if(fnmatch($serviceGroupPlace, $placeTag)) {
          $serviceGroup['icon'][0] = $serviceGroupData['icon'][0] ?? self::DefaultExpressServiceIcon[0];
          $serviceGroup['icon'][1] = $serviceGroupData['icon'][1] ?? self::DefaultExpressServiceIcon[1];
        }
      }
      if($serviceGroup) $result['serviceGroups'][$serviceGroupName] = $serviceGroup;
    }
    return $result;
  }

  function serviceGroup($serviceGroupName) {
    $knownServiceGroups = $this->config['serviceGroups'];
    if(! isset($knownServiceGroups[$serviceGroupName])) return null;
    $serviceGroup = $knownServiceGroups[$serviceGroupName];
    $result = [
      'extraInfo'      => $serviceGroup['extraInfo'] ?? null,
      'defaultIcon'    => $serviceGroup['icon'] ?? [ 'gear', 'grey' ],
      'defaultQueueId' => $serviceGroup['queueId'] ?? 65535
    ];
    $result['services'] = [];
    foreach($serviceGroup['services'] as $service) {
      if(! isset($service['queueId'])) $service['queueId'] = $serviceGroup['queueId'];
      if(! isset($service['requireMoreInfo'])) $service['requireMoreInfo'] = ( $serviceGroup['requireMoreInfo'] ?? false );
      if(! isset($service['icon'])) $service['icon'] = $serviceGroup['icon'];
      $result['services'][] = $service;
    }
    return $result;
  }

}

?>
