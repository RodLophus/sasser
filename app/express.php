<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="lib/modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="lib/modules/fontawesome-free/css/all.min.css" rel="stylesheet">
        <link href="lib/site/css/express.css" rel="stylesheet">
        <script src="lib/modules/jquery/dist/jquery.min.js"></script>
        <script src="lib/modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <script src="lib/site/js/express.js"></script>
        <title><?=Site::ExpressTitle?></title>
    </head>
    <body>
        <?php if($errMsg):?>
            <main class="d-flex flex-column justify-content-center text-center vh-100">
                <div class="position-absolute top-0 start-0 w-100 px-2 pt-3">
                    <div class="alert alert-danger" role="alert">
                    <h5 class="mb-0"><i class="fa fa-lg fa-ban me-2"></i><?=$errMsg[0]?></h5>
                    </div>
                </div>
                <div class="px-3">
                    <i class="fa fa-10x fa-frown mb-3"></i><p style="font-size: larger"><?=$errMsg[1]?></p>
                </div>
            </main>
        <?php else:?>
            <form data-step="1">
                <div id="app" class="d-flex flex-column">
                    <header class="d-flex align-items-center justify-content-center bg-dark text-white">
                        <div class="text-center">
                        <h5 class="text-nowrap mb-0"><?=$express['place'][0]?></h5>
                        <?php if($express['place'][1]): ?>
                            <small class="text-nowrap text-info"><?=$express['place'][1]?></small>
                        <?php endif ?>
                        </div>
                    </header>
                    <div class="text-center mt-1">
                        <div id="express-service-info"></div>
                        <h5 class="my-2 fw-semibold" id="step-title"></h5>
                    </div>
                    <main class="d-flex flex-column flex-grow-1 my-2">
                        <div class="form-step-1">
                            <div class="container">
                                <div class="row row-cols-1">
                                    <?php foreach($express['serviceGroups'] as $sgName => $sgData): ?>
                                        <div class="col d-flex align-items-center border py-1" data-service-group-id="<?=base64_encode($sgName)?>">
                                            <i class="fa fa-3x fa-<?=$sgData['icon'][0]?>" style="color: <?=$sgData['icon'][1]?>"></i> <span class="service-group-name"><?=$sgName?></span>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-step-2">
                            <div class="container">
                                <div class="row row-cols-1" id="express-services-list">
                                </div>
                            </div>
                        </div>
                        <div class="form-step-3 h-100">
                            <div class="container d-flex flex-column h-100">
                                <div class="d-flex flex-column flex-grow-1 mb-2">
                                    <label for="txt-extra-information" class="form-label" id="lbl-extra-information"></label>
                                    <textarea class="form-control h-100" name="extraInformation" id="txt-extra-information"></textarea>
                                    <div class="form-text" id="stt-extra-information"></div>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="anonymous" id="cb-anonymous">
                                    <label class="form-check-label" for="cb-anonymous">Relatar anonimamente</label>
                                </div>
                                <div>
                                    <input type="email" name="email" class="form-control" id="txt-email" placeholder="email@exemplo.com.br">
                                    <div class="form-text">
                                        Informe um e-mail v&aacute;lido ou marque a op&ccedil;&atilde;o <i>Relatar anonimamente</i> para continuar<br />
                                    </div>                                
                                </div>
                                <div class="form-text mt-1">
                                    <i class="fa fa-exclamation-triangle me-1"></i>Se voc&ecirc; relatar o problema anonimamente, os t&eacute;cnicos n&atilde;o poder&atilde;o entrar em contato caso tenham duvidas nem lhe avisar quando o problema for resolvido.
                                </div>
                            </div>
                        </div>
                        <div class="form-step-4">
                            <div class="container">
                                <div class="d-flex">
                                    <div class="align-self-center text-center w-100">
                                        <i class="fa fa-spin fa-8x fa-cog text-primary my-4"></i>
                                        <p>Por favor, aguarde...</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-step-5 form-step-5-success">
                            <div class="container">
                                <div class="d-flex">
                                    <div class="align-self-center text-center w-100">
                                        <i class="fa fa-8x fa-check-circle text-success mt-4 mb-1"></i>
                                        <p style="color: darkred"><strong>Ordem de servi&ccedil;o: <span id="ticket-id"></span></strong></p>
                                        <p>O problema foi relatado para a equipe t&eacute;cnica.  Em breve tomaremos provid&ecirc;ncias.</p>
                                        <p>Obrigado por sua colabora&ccedil;&atilde;o!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-step-5 form-step-5-error">
                            <div class="container">
                                <div class="d-flex">
                                    <div class="align-self-center text-center w-100">
                                        <i class="fa fa-8x fa-bomb text-danger my-4"></i>
                                        <p>Houve um erro ao relatar o problema. Por favor, tente novamente.</p>
                                        <p>Se o problema persistir, entre em contato com <?=$adminContactInfo?>.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                    <nav>
                        <div class="container d-flex mt-2 gap-2">
                                <button type="button" id="btn-back"    class="btn btn-primary flex-grow-1"><i class="fa fa-angle-left pe-2"></i>Voltar</button>
                                <button type="button" id="btn-forward" class="btn btn-primary flex-grow-half">Continuar<i class="fa fa-angle-right ps-2"></i></button>
                                <button type="button" id="btn-restart" class="btn btn-primary flex-grow-1">Informar outro problema</button>
                            </div>
                        </div>
                    </nav>
                </div>
                <input id="hdn-queue-id" type="hidden" name="queueId">
                <input id="hdn-service-description" type="hidden" name="problemDescription">
                <input id="hdn-service-group-name" type="hidden" name="serviceGroupName">
            </form>
        <?php endif;?>
    </body>
</html>
