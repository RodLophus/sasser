<div class="card my-1 text-nowrap ticket-card" >
  <div class="card-header" data-bs-toggle="collapse" data-bs-target="#card-ticket-<?=$d['id']?>">
    <div class="row row-cols-4 w-100">
      <div class="col-3 text-center border-end">
        <span>#<?php printf('%06d', $d['id']) ?></span>
        <br/>
        <span class="badge ticket-bg-<?=$d['status']?> fs-6 w-100"><?=strtoupper(Util::StatusName($d['status']))?></span>
      </div>
      <div class="col flex-grow-1">
        <span class="h4 mb-0"><?=$d['subject']?></span>
        <br/>
        &Uacute;ltima atualiza&ccedil;&atilde;o: <?=$d['lastUpdated']?> (<?=$d['elapsedTime']?><?=$d['elapsedTime'] =='hoje' ? '' : ' atr&aacute;s'?>)
      </div>
    </div>
    <div class="row row-cols-4 w-100">
      <div class="col-3 border-end">
        <br />
        Criado em: <?=$d['created']?>
      </div>
      <div class="col">
        Fila: <?=$d['queue']?><br />
        Respons&aacute;vel: <?=$d['owner']?>
      </div>
    </div>
  </div>
  <div class="collapse" data-ticket-id="<?=$d['id']?>" id="card-ticket-<?=$d['id']?>">
    <div class="card-body" >
      <span class="txt-loading">Carregando...</span>
      <ul class="list-group list-group-flush placeholder-glow"></ul>
    </div>
  </div>
</div>
