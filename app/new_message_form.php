<div class="card-body">
  <div class="mb-3">
    <label for="text" class="form-label">Mensagem</label>
    <textarea class="form-control" id="main-form-text" name="text" rows="5" required></textarea>
    <div class="invalid-feedback"><i class="fa fa-times-circle pe-1"></i>Este campo &eacute; obrigat&oacute;rio!</div>
    <div class="form-text">Escreva a mensagem que deseja enviar para a pessoa respons&aacute;vel por atender esta solicita&ccedil;&atilde;o.</div>
  </div>
  <div class="mb-3">
    <div class="input-group">
      <input type="text" class="form-control" id="main-form-file-name" readonly>
      <input type="file" class="form-control" id="main-form-file" name="file[]" multiple style="display:none">
      <label class="btn btn-outline-secondary" for="main-form-file-name" id="btn-main-form-reset-file"><span class="fa fa-times-circle"></span></label>
      <label class="btn btn-outline-secondary" for="main-form-file">Selecionar arquivo</label>
    </div>
    <div class="form-text">Se achar necess&aacute;rio, voc&ecirc; pode anexar arquivos &agrave; mensagem.  Fotos e capturas de tela podem ser &uacute;teis no diagn&oacute;stico do seu problema.</div>
  </div>
</div>
