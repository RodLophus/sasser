<div class="card-body">
  <div class="mb-3">
    <label for="email" class="form-label">E-mail(s) de contato</label>
    <input type="text" class="form-control validate-email-list warn-no-requestor-email" name="email" value="<?=$_SESSION['emailAddresses'][0]?>" required>
    <div class="warning-feedback"></div>
    <div class="invalid-feedback"></div>
    <div class="form-text">Voc&ecirc; pode informar v&aacute;rios endere&ccedil;os de e-mail, separados por espa&ccedil;os ou v&iacute;rgulas.  As mensagens a respeito desta solicita&ccedil;&atilde;o ser&atilde;o enviadas para todos os endere&ccedil;os de e-mail relacionados acima.</div>
  </div>
  <div class="mb-3">
    <label for="subject" class="form-label">Assunto</label>
    <input type="text" class="form-control" id="main-form-subject" name="subject" required>
    <div class="invalid-feedback"><i class="fa fa-times-circle pe-1"></i>Este campo &eacute; obrigat&oacute;rio!</div>
    <div class="form-text">Informe resumidamente do se se trata o problema.  O assunto ajuda a encaminhar a sua solicita&ccedil;&atilde;o para o t&eacute;cnico adequado dentro da &aacute;rea.</div>
  </div>
  <div class="mb-3">
    <label for="text" class="form-label">Mensagem</label>
    <textarea class="form-control" id="main-form-text" name="text" rows="5" required></textarea>
    <div class="invalid-feedback"><i class="fa fa-times-circle pe-1"></i>Este campo &eacute; obrigat&oacute;rio!</div>
    <div class="form-text">Descreva o problema da melhor forma que puder.  Detalhes podem agilizar o seu atendimento.</div>
  </div>
  <div class="mb-3">
    <div class="input-group">
      <input type="text" class="form-control" id="main-form-file-name" readonly>
      <input type="file" class="form-control" id="main-form-file" name="file[]" multiple style="display:none">
      <label class="btn btn-outline-secondary" for="main-form-file-name" id="btn-main-form-reset-file"><span class="fa fa-times-circle"></span></label>
      <label class="btn btn-outline-secondary" for="main-form-file">Selecionar arquivo</label>
    </div>
    <div class="form-text">Se achar necess&aacute;rio, você pode anexar arquivos &agrave; solicita&ccedil;&atilde;o.  Fotos e capturas de tela podem ser &uacute;teis no diagn&oacute;stico do seu problema.</div>
  </div>
</div>
