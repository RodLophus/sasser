<?php
  $icon = array_key_exists('icon', $d) ?
    '<i class="fas ' . $d['icon'] . ' pe-2"></i>' :
    '<span class="placeholder placeholder-lg me-1" style="width: 1.5em;"></span>';
  $date = $d['date'] ?? '<span class="placeholder placeholder-lg" style="width:  10em;"></span>';
  $text1 = $d['text'][0] ?? '<span class="placeholder" style="width: 10em;"></span>';
  $text2 = $d['text'][1] ?? '<span class="placeholder" style="width: 20em;"></span>';
?>

<li class="list-group-item <?=isset($d['clickable']) ? ' transaction-link' : ''?>" data-transaction-id="<?=$d['id']?>" id="ticket-transaction-list-item-<?=$d['id']?>">
  <?=$icon?><?=$date?><br/>
  <strong><?=$text1?></strong> <?=$text2?>
</li>
