<?php

spl_autoload_register(function ($className) {
  require_once('app/lib/' . $className . '.php');
});

$config = new Config('config');

$query = $_POST['query'] ?? null;
$param = $_POST['param'] ?? null;
$modifier = $_POST['modifier'] ?? null;
$username = $_POST['username'] ?? null;
$password = $_POST['password'] ?? null;

$pageMode = 'main';

session_start();

$ldap = new Ldap($config->ldap());
$rt = new RTRest($config->rt());

if(! isset($_SESSION['displayName'])) {
  // Tenta autenticacao via Phoenix se for fornecido um token via GET
  if(isset($_GET['token'])) {
    $token = $_GET['token'];
    $phoenix = new Phoenix($config->phoenix());
    if($phoenixUserData = $phoenix->decrypt($token)) {
      if($userDetails = $ldap->getUser($phoenixUserData['imecc']['uid'])) {
        $_SESSION['displayName'] = $userDetails->name;
        $_SESSION['emailAddresses'] = $userDetails->emailAddresses;
      } else {
        $_SESSION['displayName'] = $phoenixUserData['uec']['name'];
        $_SESSION['emailAddresses'] = array($phoenixUserData['uec']['email']);
      }
      $_SESSION['rtUserIds'] = $rt->getUsersIds(array('EmailAddress' => $_SESSION['emailAddresses']));
      $_SESSION['logoutUrl'] = $config->phoenix('logout_url');
      $pageMode = 'main';
    }
  } else {
    $phoenixAuthUrl = $config->phoenix('url') . '?redirect_uri=' . $config->common('base_url');
    $pageMode = 'login';
  }
}

if(! isset($_SESSION['ticketStatusFilter'])) {
  $_SESSION['ticketStatusFilter'] = Config::DefaultTicketStatusFilter;
}

if(is_numeric($attachmentId = $_GET['attachment'] ?? null)) {
  // Solicitado download de um anexo.
  // Verifica se o anexo esta em um ticket solicitado pelo usuario logado
  if($attachment = $rt->getAttachment($attachmentId)) {
    $transaction = $rt->getTransaction($attachment->TransactionId->id);
    $ticket = $rt->getTicket($transaction->Object->id);
    // Procura pelo ID do usuario logado dentre os solicitantes do ticket
    foreach($ticket->Requestor as $user) {
      if(in_array($user->id, $_SESSION['rtUserIds'])) {
        header('Content-Type: ' . $attachment->ContentType);
        header('Content-Disposition: attachment; filename=' . $attachment->Filename);
        header('Pragma: no-cache');
        print(base64_decode($attachment->Content));
        exit;
      }
    }
  }
  print('Arquivo nao disponivel.');
  exit;
}

if($query) {
  if($pageMode == 'main') {
    $d = array();
    switch($query) {
      case 'tickets':
        $filter = array();
        $filter['Requestor'] = $_SESSION['emailAddresses'];
        if(is_numeric($param)) {
          // Parametro eh numerico: eh o ID de um ticket
          $filter['ID'] = $param;
        } else {
          $filter['Status'] = $param;
          $_SESSION['ticketStatusFilter'] = $param;
        }
        $tickets = $rt->getTickets($filter, $modifier ? $modifier : 1);
        print(json_encode(array(
          'tickets' => array_map(function($k){ return $k->id; }, $tickets->items),
          'pages' => $tickets -> pages
        )));
        break;
      case 'ticket':
        $t = $rt->getTicket($param);
        if(! $t) break;
        $d = array();
        $owner = $t->Owner->id;
        if($owner == 'Nobody') {
          $d['owner'] = '(n&atilde;o atribu&iacute;do)';
        } else {
          if($user = $ldap->getUser($owner)) {
            $d['owner'] = "$user->name ($user->group)";
          }
        }
        $d['id']      = $t->id;
        $d['status']  = $t->Status;
        $d['created'] = date("d/m/Y", strtotime($t->Created));
        $d['subject'] = $t->Subject;
        $d['queue']   = $rt->getQueue($t->Queue->id)->Name;
        $d['lastUpdated'] = date("d/m/Y", strtotime($t->LastUpdated));
        $d['elapsedTime'] = Util::ElapsedTime($t->LastUpdated);
        require('app/ticket_card.php');
        break;
      case 'transactions-placeholders':
        $filter = array(
          'Type' => array('Create', 'Correspond', 'Status', 'Set')
        );
        $transactions = $rt->getTicketTransactions($param, $filter)->items;
        $transactionsIds = array_map(function($k){ return $k->id; }, $transactions);
        sort($transactionsIds, SORT_NUMERIC);
        foreach($transactionsIds as $transactionId) {
          $d['id'] = $transactionId;
          require('app/ticket_history.php');
        }
        break;
      case 'transaction':
        $t = $rt->getTransaction($param);
        if(! $t) break;
        $d = array();
        $d['date'] = date('d/m/Y - H:i:s', strtotime($t->Created));
        $d['id'] = $t->id;
        switch($t->Type) {
          case 'Create':
            $d['icon'] = 'fa-concierge-bell';
            $d['text'] = array('Criado por:', $rt->getUser($t->Creator->id)->RealName ?? $t->Creator->id);
            $d['clickable'] = true;
            break;
          case 'Correspond':
            $d['icon'] = 'fa-envelope';
            $d['text'] = array('Mensagem de:', $rt->getUser($t->Creator->id)->RealName ?? $t->Creator->id);
            $d['clickable'] = true;
            break;
          case 'Status':
            $d['icon'] = 'fa-traffic-light';
            $d['text'] = array('Novo estado:', Util::StatusName($t->NewValue));
            break;
          case 'Set':
            switch($t->Field) {
              case 'Owner':
                $d['icon'] = 'fa-user-check';
                $d['text'] = array('Novo respons&aacute;vel:', $rt->getUser($t->NewValue)->RealName);
                break;
              case 'Queue':
                $d['icon'] = 'fa-cogs';
                $d['text'] = array('Fila alterada:', $rt->getQueue($t->OldValue)->Name . ' &#10140; ' . $rt->getQueue($t->NewValue)->Name);
                break;  
              case 'Subject':
                $d['icon'] = 'fa-edit';
                $d['text'] = array('Assunto alterado:', '<i>"' . $t->OldValue . '"</i> &#10140; <i>"' . $t->NewValue . '"</i>');
                break;
              default:
                $d['icon'] = 'fa-exchange-alt';
                $d['text'] = array('Campo <i>"' . $t->Field . '"</i> alterado:', 'de: <i>"' . $t->OldValue . '"</i> para: <i>"' . $t->NewValue . '"</i>');
            }
            break;
        }
        require('app/ticket_history.php');
        break;
      case 'transaction-message':
        $d = array();
        $d['cc'] = '';
        $d['files'] = array();
        $contentPlain = '';
        $contentHtml = '';
        foreach($rt->getTransactionAttachments($param)->items as $ta) {
          if($attachment = $rt->getAttachment($ta->id)) {
            if(isset($attachment->Headers)) {
              if(preg_match('/RT-Send-CC:[\s]*(.+)\n/', $attachment->Headers, $matches)) {
                $d['cc'] = $matches[1];
              }
            }
            if(isset($attachment->Filename)) {
              $d['files'][] = array(
                'id' => $ta->id,
                'name' => $attachment->Filename,
                'type' => $attachment->ContentType
              );
            } else {
              if($attachment->Content) {
                switch($attachment->ContentType) {
                  case 'text/plain':
                    $contentPlain = $attachment->Content;
                    break;
                  case 'text/html':
                    $contentHtml = $attachment->Content;
                    break;
                }
              }
            }
          }
        }
        $d['content'] = $contentHtml ? base64_decode($contentHtml) : '<pre>' . base64_decode($contentPlain) . '</pre>';
        $d['content'] = strip_tags($d['content'], ['p', 'i', 'b', 'br', 'ul', 'ol', 'li', 'strong', 'pre', 'blockquote']);
        print(json_encode($d));
        break;
      case 'form':
        require('app/' . $param . '_form.php');
        break;
      case 'logout':
        $logoutUrl = $_SESSION['logoutUrl'] ?? $config->common('base_url');
        session_unset();
        session_commit();
        print(json_encode(array(
          'redirect' => $logoutUrl
        )));
    } // switch
  } // if($pageMode == 'main')
} else {
  // 'query' nao informado no 'post': nao eh ajax
  $message = [];
  $messageText = '';
  if(isset($_POST['form_mode'])) {
    switch($_POST['form_mode']) {
      case 'new-request':
        $data = array(
          'Queue'     => $_POST['queue'],
          'Requestor' => explode(' ', $_POST['email']),
          'Subject'   => $_POST['subject'],
          'ContentType' => 'text/plain',
          'Content'   => $_POST['text'] . PHP_EOL . PHP_EOL .
            '-----' . PHP_EOL .
            'Criado por ' . $_SESSION['emailAddresses'][0]
        );
        $files = $_FILES['file'];
        for($n = 0; $n < count($files['name']); $n++) {
          if(($files['size'][$n] > 0) && is_uploaded_file($files['tmp_name'][$n])) {
            $data['Attachments'][] = array(
              'FileName'    => $files['name'][$n],
              'FileType'    => $files['type'][$n],
              'FileContent' => base64_encode(file_get_contents($files['tmp_name'][$n]))
            );
          }
        }
        if($ticket = $rt->createTicket($data)) {
          $message = array(
            'text' => sprintf('Solicita&ccedil;&atilde;o criada: #%06d', $ticket->id),
            'type' => 'success'
          );
        } else {
          $message = array(
            'text' => 'Erro criando a solicita&ccedil;&atilde;o.  Por favor contacte o administrador do sistema',
            'type' => 'danger'
          );
        }
        break;
      case 'new-message':
        if(is_numeric($ticketId = $_POST['ticket'])) {
          $data = array(
            'ContentType' => 'text/plain',
            'Content'   => $_POST['text']
          );
          $files = $_FILES['file'];
          for($n = 0; $n < count($files['name']); $n++) {
            if(($files['size'][$n] > 0) && is_uploaded_file($files['tmp_name'][$n])) {
              $data['Attachments'][] = array(
                'FileName'    => $files['name'][$n],
                'FileType'    => $files['type'][$n],
                'FileContent' => base64_encode(file_get_contents($files['tmp_name'][$n]))
              );
            }
          }  
          if($rt->replyTicket($_POST['ticket'], $data)) {
            $message = array(
              'text' => sprintf('Mensagem enviada (solicita&ccedil;&atilde;o #%06d)', $ticketId),
              'type' => 'success'
            );
          } else {
            $message = array(
              'text' => 'Erro enviando mensagem.  Por favor contacte o administrador do sistema',
              'type' => 'danger'
            );
          }
        }
        break;
      case 'login':
        if($username && $password) {
          if(pam_auth($username, $password)) {
            if($userDetails = $ldap->getUser($username)) {
              $_SESSION['displayName'] = $userDetails->name;
              $_SESSION['emailAddresses'] = $userDetails->emailAddresses;
              $_SESSION['rtUserIds'] = $rt->getUsersIds(array('EmailAddress' => $userDetails->emailAddresses));
              $pageMode = 'main';
            } else {
              $message = array(
                'text' => 'Erro carregando as informa&ccedil;&otilde;es do usu&aacute;rio',
                'type' => 'danger'
              );  
            }
          } else {
            $message = array(
              'text' => 'Nome de usu&aacute;rio ou senha incorreta',
              'type' => 'danger'
            );
          }
        } else {
          $message = array(
            'text' => 'Informe seu nome de usu&aacute;rio e senha.',
            'type' => 'danger'
          );
        }
        break;
    }
    if(! empty($message)) {
      switch($message['type']) {
        case 'success' : $message['icon'] = 'check-circle'; break;
        case 'danger'  : $message['icon'] = 'times-circle'; break;
      }
    }
  }
  require('app/canvas.php');
}

?>
