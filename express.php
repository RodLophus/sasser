<?php

spl_autoload_register(function ($className) {
  require_once('app/lib/' . $className . '.php');
});

$config = new Config('config');

$placeTag       = $_GET['l'] ?? null;
$placeVariation = $_GET['v'] ?? null;
$serviceGroupId = $_POST['serviceGroupId'] ?? null;
$problemDescription = $_POST['problemDescription'] ?? null;

$express = $config->express($placeTag, $placeVariation);

$errMsg = [];

if($problemDescription) {
  if(isset($_POST['queueId'])) {
    $rt = new RTRest($config->rt());
    $result = array('error' => true);
    $queueId = $_POST['queueId'];
    $email = $_POST['email'] ?? null;
    $serviceGroupName = $_POST['serviceGroupName'];
    $extraInformation = $_POST['extraInformation'];
    $ticketSubject = "$serviceGroupName - {$express['place'][0]} {$express['place'][1]}";
    if($ticket = $rt->createTicket(array(
      'Queue'     => $queueId,
      'Requestor' => $email ?? $config->common('anonymous_email'),
      'Subject'   => html_entity_decode($ticketSubject, ENT_QUOTES, 'UTF-8'),
      'ContentType' => 'text/html',
      'Content' => '<!DOCTYPE html><html><head><meta charset="utf-8"></head><body>' .
        "<h4>$ticketSubject</h4><hr />" .
        '<h5>Descri&ccedil;&atilde;o do problema:</h5><p>' . nl2br(htmlentities($problemDescription, ENT_QUOTES, 'UTF-8')) . '</p><hr />' .
        ($extraInformation ? '<h5>Informa&ccedil;&otilde;es adicionais:</h5>' . nl2br(htmlentities($extraInformation, ENT_QUOTES, 'UTF-8')) . '</p><hr />' : '') .
        ($email ? '' : '<h5><strong>N&atilde;o responda a este e-mail!!</strong></h5>O solicitante n&atilde;o informou um e-mail para contato</p>') .
        '</body></html>'
    ))) {
      $result = array(
        'error' => false,
        'ticketId' => $ticket->id,
      );
    }
    exit(json_encode($result));
  }
}

if($serviceGroupId) {
  $data = $config->serviceGroup(base64_decode($serviceGroupId));
  exit(json_encode($data ?? []));
}

if($placeTag) {
  if($express) {
    $adminContactInfo = $config->common('admin_contact_info');
  } else {
    $errMsg = [
      'Funcionalidade n&atilde;o dispon&iacute;vel',
      'Desculpe. A solicita&ccedil;&atilde;o de servi&ccedil;os via QRCode ainda n&atilde;o est&aacute; dispon&iacute;vel para esta sala ou equipamento.'
    ];
  }
  require('app/express.php');
  exit();
}

header('Location: ' . $config->common('base_url'));