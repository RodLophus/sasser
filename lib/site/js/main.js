$(document).ready(function() {
  var timerSelectFilterOptions = null
  var timerDismissAlert = null

  // Seletor de filtros de tickets
  $('#tickets-filters .badge').click(function() {
    window.clearTimeout(timerSelectFilterOptions)
    if($(this).data('state')) {
      $(this).data('state', 0)
      $(this).children('.icon').removeClass('fa-eye').addClass('fa-eye-slash')
    } else {
      $(this).data('state', 1)
      $(this).children('.icon').removeClass('fa-eye-slash').addClass('fa-eye')
    }
    // Recarrega a lista de tickets caso o usuario fique 1 segundo sem alterar os filtros
    timerSelectFilterOptions = window.setTimeout(function() {
      $('#tickets-filters .card').collapse('hide')
      getTickets()
    }, 1000)
  })

  // Abre o formulario de criacao de nova solicitacao
  $('#accordion-new-ticket li').click(function() {
    showMainForm('new_request', {
      'queue' : $(this).data('new-ticket-queue'),
      'subject' : $(this).data('new-ticket-subject')
    })
  })

  // Fecha o formulario principal
  $('#btn-main-form-back').click(function() {
    $('.main-content').hide()
    $('.accordion-collapse').collapse('hide')
    switch($('#main-form-mode').val()) {
      case 'new-request':
        $('#main-new-ticket').show()
        break
      case 'new-message':
        $('#main-ticket-list').show()
    }
  })

  // Botao para carregar mais tickets na lista principal
  $('#ticket-list-load-more button').click(function() {
    var nextPage = $('#ticket-list-load-more').data('next-page')
    if(nextPage > 0) {
      getTickets(nextPage)
    }
  })

  // Link de "logout"
  $('#logout-link').click(function() {
    $.post('', {
      query : 'logout'
    }, function(data) {
      window.location.replace(data.redirect);
    }, 'json')
  })

  // Muda o conteudo do painel principal conforme o botao clicado na barra lateral
  $('.sidebar .nav-link').click(function() {
    var target = $(this).data('show');
    $('.main-content').hide()
    if(target == 'new-ticket') $('.accordion-collapse').collapse('hide')
    // Recarrega a pagina se clicar no botao "Inicio" quando ja estiver na pagina inicial
    if((target == 'ticket-list') && $(this).hasClass('active'))
      window.location.href = window.location.href.split('?')[0]
    $(`#main-${target}`).show()
    $('.sidebar a').removeClass('active')
    $(this).addClass('active')
  })

  // Remove o alerta apos 3 segundos
  if($('#alert').length) {
    timerDismissAlert = window.setTimeout(function() {
      var alert = new bootstrap.Alert($('#alert'))
      alert.close()
    }, 5000)
  }

  // Abre o texto de transacoes dos tickets
  $(document).on('click', '.transaction-link', function() {
    showMessageViewer($(this).data('transaction-id'))
  })

  // Abre o formulario para enviar mensagem para o responsavel pelo ticket
  $(document).on('click', '.new-ticket-message', function() {
    showMainForm('new_message', { 'ticket' : $(this).data('ticket') })
  })

  // Carrega o historico do ticket quando o corpo do card eh expandido
  $(document).on('show.bs.collapse', '#main-ticket-list-contents .collapse', getTicketHistory)

  // Validacao dos formularios
  $(document).on('submit', 'form.custom-form-validation', function(event) {
    var targetForm = event.target
    $(targetForm).find('.validate-email-list').each(function() {
      var field = $(this)
      var emailAddresses = field.val().split(/[ ,]+/).filter(e => e.length > 0)
      var errorMessage = null
      if(emailAddresses.length == 0) {
        errorMessage = 'Este campo &eacute; obrigat&oacute;rio!'
      } else {
        field.val(emailAddresses.join(' '))
      }
      emailAddresses.forEach(function(emailAddress) {
        if(! emailAddress.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
          errorMessage = `Endere&ccedil;o de e-mail inv&aacute;lido: "${emailAddress}"`
        }
      })
      if(errorMessage) {
        field.siblings('.invalid-feedback').html(`<i class="fa fa-times-circle pe-1"></i>${errorMessage}`)
        field.removeClass('is-valid').addClass('is-invalid')
      } else {
        field.removeClass('is-invalid').addClass('is-valid')
      }
    })
    if (! targetForm.checkValidity()) {
      event.preventDefault()
      event.stopPropagation()
    }
    $(targetForm).addClass('was-validated')
  })

  // Mensagem de aviso quando o requisitante exclui o proprio endereco de um campo
  $(document).on('input', 'input.warn-no-requestor-email', function() {
    var emailAddresses = $(this).val().split(/[ ,]+/)
    var requestorEmailAddresses = $('#user-email-addresses').data('value').split(':')
    if(emailAddresses.filter(x => requestorEmailAddresses.includes(x)).length == 0) {
      $(this).siblings('.warning-feedback').html('<i class="fa fa-exclamation-circle pe-1"></i>Voc&ecirc; n&atilde;o poder&aacute; acompanhar o andamento desta solicita&ccedil;&atilde;o caso n&atilde;o inclua um dos seus endere&ccedil;os de e-mail na lista de contatos!')
    } else {
      $(this).siblings('.warning-feedback').empty()
    }
  })

  if(urlParameter('ticket')) {
    $('.sidebar .nav-link').addClass('active-no-decoration')
  }

  // Carrega a lista inicial de tickets
  getTickets()

})



// Visualizador de mensagem modal
function showMessageViewer(id) {
  var dlg = $('#modal-message-viewer')
  var modalMessageViewer = new bootstrap.Modal(dlg)
  dlg.find('.message-attachment-list').hide()
  dlg.find('.message-attachment-list ul').empty()
  dlg.find('.message-viewer-subtitle i').empty()
  dlg.find('.message-body').empty()
  dlg.find('.message-body-spinner').show()
  $.post('', {
    query : 'transaction-message',
    param : id
  }, function(data) {
    if(data.cc) {
      dlg.find('.message-viewer-subtitle i').html(`Enviada com c&oacute;pia para: ${data.cc}`)
    }
    if(data.files.length > 0) {
      dlg.find('.message-attachment-list > button').html(`Anexos (${data.files.length})`)
      data.files.forEach(function(file) {
        dlg.find('.message-attachment-list ul').append(`<li><a class="dropdown-item message-attachment-download" href="?attachment=${file.id}">${file.name}</a></li>`)
      })
      dlg.find('.message-attachment-list').show()
    }
    dlg.find('.message-body').html(data.content)
    dlg.find('.message-body-spinner').hide()  
  }, 'json')
  modalMessageViewer.show()
}


// Caixa de dialogo modal
function showDialog(params, callbackFunction = null) {
  var body = params['body']
  var mode = params['mode'] || 'warning'
  var buttons = params['buttons'] || [ 'Ok', 'Cancelar' ]
  var dialogMode = {
    error   : { title : 'Erro',                     icon : 'times-circle',       color : 'text-danger'  },
    warning : { title : 'Aten&ccedil;&atilde;o',    icon : 'exclamation-circle', color : 'text-warning' },
    info    : { title : 'Informa&ccedil;&atilde;o', icon : 'info-circle',        color : 'text-primary' },
  }
  if(! dialogMode.hasOwnProperty(mode)) return
  var dlg = $('#modal-dialog')
  var dlgButtonConfirm = dlg.find('#modal-dialog-confirm-button')
  var dlgButtonAbort = dlg.find('#modal-dialog-abort-button')
  dlg.find('.modal-title i').attr('class', `me-2 fa-2x fa fa-${dialogMode[mode]['icon']} ${dialogMode[mode]['color']}`)
  dlg.find('.modal-title span').attr('class', dialogMode[mode]['color']).html(dialogMode[mode]['title'])
  dlg.find('.modal-body').html(body)
  dlgButtonConfirm.text(buttons[0])
  if(buttons.length > 1) {
    dlgButtonAbort.text(buttons[1])
    dlgButtonAbort.show()
  } else {
    dlgButtonAbort.hide()
  }
  // Funcao de callback eh chamada apenas quando o usuario clica no botao "confirm"
  dlgButtonConfirm.off('click')
  if(typeof(callbackFunction) == 'function') {
    dlgButtonConfirm.click(callbackFunction)
  }
  var modalDialog = new bootstrap.Modal(dlg)
  modalDialog.show()
}


// Retorna um vetor com os atributos de filtragem selecionados
function getTicketsFilter() {
  var ticketsFilter = []
  $('#tickets-filters .badge').each(function() {
    if($(this).data('state')) {
      // O nome do estado eh a ultima parte (separada por "-") do ID do badge
      ticketsFilter.push($(this).attr('id').split('-').pop())
    }
  })
  return(ticketsFilter)
}


// Atualiza a lista de tickets
function getTickets(page = 1) {
  var searchParams = new URLSearchParams(window.location.search)
  $('#ticket-list-load-more button').data('next-page', 0)
  $('#ticket-list-load-more').hide()
  $('#ticket-list-load-progress .card-header').html('Carregando...')
  $('#ticket-list-load-progress .progress-bar').attr('style', 'width : 0%;')
  $('#ticket-list-load-progress').show()
  // Obtem uma lista com os IDs dos tickets, conforme o filtro selecionado pelo usuario
  $.post('', {
    query : 'tickets',
    param : ((t = urlParameter('ticket')) ? t : getTicketsFilter()),
    modifier : page
    }, function(data) {
      var ticketIds = data.tickets
      var pages = data.pages
      var ticketCount = ticketIds.length
      var n = 1
      if(ticketIds.length == 0) {
        $('#ticket-list-load-progress').hide()
        $('#main-ticket-list-contents').append('<div class="card my-1"><div class="card-body text-center h4">' + 
          (searchParams.has('ticket') ? 'Solicita&ccedil;&atilde;o n&atilde;o encontrada.' : 'Nenhuma solicita&ccedil;&atilde;o encontrada.') + 
          '</div></div>')
        return
      }
      if(page == 1) {
        $('#main-ticket-list-contents').find('.ticket-card').remove()
      }
      // Cria "placeholders" onde os cards contendo o resumo dos tickets serao colocados
      // Para garantir a ordem cronologica, visto que os conteudos dos cards serao obtidos na ordem que o servidor responder
      ticketIds.forEach(function(ticketId) {
        // Placeholders sao "divs" vazios
        $('#main-ticket-list-contents').append(`<div id="ticket-card-placeholder-${ticketId}"></div>`)
      })
      ticketIds.forEach(function(ticketId) {
        // Obtem o conteudo do card do ticket
        $.post('', {
          query : 'ticket',
          param : ticketId
        }, function(data) {
          var progress = ((n / ticketCount) * 100).toFixed(0)
          $('#ticket-list-load-progress .card-header').html(`Carregando: ${n} de ${ticketCount}`)
          $('#ticket-list-load-progress .progress-bar').attr('style', `width : ${progress}%;`)
          // Popula os cards criados anteriormente
          $(`#ticket-card-placeholder-${ticketId}`).replaceWith(data)
          // Rola a lista de tickets ate o final
          $('#main-ticket-list').scrollTop($('#main-ticket-list').prop('scrollHeight'))
          if(n == ticketCount) {
            // Lista de tickets carregada.  Reinicia a barra de progresso
            $('#ticket-list-load-progress').hide()
            // Se foi passado o numero de um ticket na busca, expande os detalhes do card automaticamente
            if(searchParams.has('ticket')) {
              $('#main-ticket-list-contents .collapse').collapse('show')
            }
            if(page < pages) {
              $('#ticket-list-load-more').data('next-page', page + 1)
              $('#ticket-list-load-more').show();
            } else {
              $('#ticket-list-load-more').hide();
            }
          }
          n++
        })
      })
    }, 'json')
}


// Obtem o historico do ticket
function getTicketHistory() {
  var target = $(this)
  var ticketId = target.data('ticket-id')
  if(! target.data('updated')) {
    target.data('updated', '1')
    // Obtem uma lista dos eventos do historico do ticket (mais rapido do que obter o evento).
    // Os eventos estao em divs com IDs HTML contento o ID do evento e placeholders no lugar dos dados
    $.post('', {
      query : 'transactions-placeholders',
      param : ticketId
    }, function(data) {
      target.find('.txt-loading').remove()
      target = target.find('.list-group')
      target.html(data)
      target.append(`<li class="list-group-item new-ticket-message" data-ticket="${ticketId}"><button type="button" class="btn btn-outline-danger"><i class="fa fa-comments fa-2x pe-2"></i>Enviar mensagem sobre esta solicita&ccedil;&atilde;o</button></li>`)
      target.children('.list-group-item').each(function() {
        var transactionId = $(this).data('transaction-id')
        $.post('', {
          query : 'transaction',
          param : transactionId
        }, function(data) {
          // Troca os placeholders pelo conteudo real
          $(`#ticket-transaction-list-item-${transactionId}`).replaceWith(data)
        })
      })
    })
  }
}


// Abre o formulario principal
function showMainForm(formName, param) {
  switch(formName) {
    case 'new_request':
      formSubject = 'Nova Solicita&ccedil;&atilde;o'
      formMode = 'new-request'
      break;
    case 'new_message':
      formSubject = 'Nova Mensagem'
      formMode = 'new-message'
      break;
    default:
      return
  }
  $('.main-content').hide()
  $('#main-form form').removeClass('was-validated')
  $('#main-form form').off('submit')
  $('#btn-main-form-reset-file').off('click')
  $('#main-form-file').off('change')
  $.post('', {
    query : 'form',
    param : formName
  }, function(data) {
    $('#main-form-body').html(data)
    $('#main-form-title').html(formSubject)
    $('#main-form-subject').val(param.subject ?? null)
    $('#main-form-new-ticket-queue').val(param.queue ?? null)
    $('#main-form-new-message-ticket').val(param.ticket ?? null)
    $('#main-form-mode').val(formMode)
    // Atualiza o campo de texto que contem o nome dos arquivos anexados
    // quando novos arquivos forem selecionados
    $('#main-form-file').change(function(e) {
      var files = e.target.files;
      if(files.length > 1) {
        $('#main-form-file-name').val(`(${files.length} arquivos)`)
      } else {
        $('#main-form-file-name').val(files[0].name)
      }
      $('#btn-main-form-reset-file').show()
    })
    // Botao para remover os arquivos anexados
    $('#btn-main-form-reset-file').click(function() {
      $('#main-form-file').val('')
      $('#main-form-file-name').val('')
      $('#btn-main-form-reset-file').hide()
    })
    $(formMode == 'new-message' ? '#main-form-text' : '#main-form-subject').focus()
  })
  $('#main-form').show()
}


// Retorna o valor de um parametro da URL
function urlParameter(name){
  var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
  if (results == null){
     return null;
  }
  else{
     return results[1] || 0;
  }
}

