$(document).ready(function() {
    $('#login-form-username').focus()
    $('#login-form input').on('input', function() {
        $('#login-form button').prop('disabled', ! ($('#login-form-username').val().length && $('#login-form-password').val().length))
    })
})