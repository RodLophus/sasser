$(function() {
    $('#app').height(innerHeight)

    formStep(0)

    $('.form-step-1 .col').click(function() {
        $('#hdn-service-group-name').val($(this).children('.service-group-name').text())
        loadServices($(this).data('service-group-id')).then(formStep(1))
    })

    $('#express-services-list').on('click', '.col', function() {
        $('#hdn-queue-id').val($(this).data('queue-id'))
        $('#hdn-service-description').val($(this).children('#service-description').text())
        $('form').data('require-more-info', $(this).data('require-more-info'))
        formStep(1)
    })

    $('#btn-restart').click(function() {
        formStep(0)
    })

    $('#btn-back').click(function() {
        formStep(-1)
    })

    $('#btn-forward').click(function() {
        formStep(1)
        if(currentFormStep() == 4) {
            $.post('', $('form').serialize(), function(data) {
                if(data.error) {
                    formStep(1, 'error')
                } else {
                    $('#ticket-id').text(data.ticketId)
                    formStep(1, 'success')
                }
                
            }, 'json')
        }
    })

    $('#cb-anonymous').change(function() {
        $('#txt-email').prop('disabled', $(this).prop('checked'))
    })

    $('#txt-extra-information, #cb-anonymous, #txt-email').on('input', checkForm)
})


function currentFormStep() {
    return parseInt($('form').data('step'))
}


function requireExtraInfo() {
    return $('form').data('require-more-info')
}


function loadServices(serviceGroupId) {
    return new Promise(function(done) {
        $.post('', { serviceGroupId: serviceGroupId }, function(data) {
            $('#express-services-list').empty()
            $('#express-service-info').empty()
            if(data.extraInfo) {
                $('#express-service-info').html(`
                    <div class="d-flex justify-content-center align-items-center alert alert-info mb-1 py-1" role="alert">
                        <i class="fa fa-tools text-primary me-2" style="font-size: 2.5em"></i>
                        <div><strong>${data.extraInfo[0]}</strong><br />${data.extraInfo[1]}</div>
                    </div>
                `)
            }
            data.services.forEach(function(service) {
                $('#express-services-list').append(`
                    <div class="col d-flex align-items-center border py-1" data-queue-id="${service.queueId}" data-require-more-info="${service.requireMoreInfo}">
                        <i class="fa fa-3x fa-${service.icon[0]}" style="color: ${service.icon[1]}"></i> <span id="service-description">${service.description}</span>
                    </div>
                `)
            })
            done()
        }, 'json')
    })
}


function formStep(delta, variant = null) {
    const stepTitles = {
        1: 'O que est&aacute; com problema?',
        2: 'Sobre o que &eacute; o problema?',
        3: 'Informa&ccedil;&otilde;es adicionais',
        4: 'Relatando o problema',
        5: { success: 'Problema relatado!', error: 'Erro relatando o problema' }
    }
    var stepNumber
    if(delta == 0) {
        stepNumber = 1
        $('#txt-extra-information').val('')
    } else {
        stepNumber = currentFormStep() + delta
    }
    $('form').data('step', stepNumber)
    $('#step-title').html(variant? stepTitles[stepNumber][variant] : stepTitles[stepNumber])
    for(i = 1; i < 6; i++) {
        if(stepNumber == i) {
            $(`.form-step-${i}` + (variant ? `-${variant}` : '')).show()
        } else {
            $(`.form-step-${i}`).hide()
        }
    }
    $('#express-service-info, #btn-back, #btn-forward, #btn-restart').hide()
    switch(stepNumber) {
        case 2:
            $('#express-service-info').show()
        case 1:
            $('#btn-back').show()
            break
        case 4:
            $('#btn-restart').show().prop('disabled', true)
            break
        case 5:
            $('#btn-restart').show().prop('disabled', false)
            break
        case 3:
            if(requireExtraInfo()) {
                $('#lbl-extra-information').html('Descreva o problema')
                $('#stt-extra-information').html('<i class="fa fa-exclamation-triangle me-1"></i>Digite pelo menos duas palavras!')
            } else {
                $('#lbl-extra-information').html('Detalhes do problema')
                $('#stt-extra-information').html('Escreva aqui quaisquer informa&ccedil;&otilde;es que possam ajudar na solu&ccedil;&atilde;o do problema')
            }
            checkForm()
        default:
            $('#btn-back').show()
            $('#btn-forward').show()
    }
    $('#btn-back').prop('disabled', stepNumber == 1)
}


function checkEmailAddress(emailAddress) {
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailAddress)
}


function checkForm() {
    var formOk = $('#cb-anonymous').prop('checked') || checkEmailAddress($('#txt-email').val()) > 0
    if(requireExtraInfo()) formOk &&= ($('#txt-extra-information').val().split(/[\s]/).filter(String).length > 1)
    $('#btn-forward').prop('disabled', ! formOk)
}
